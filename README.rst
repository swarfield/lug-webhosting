LUG Webhosting presentation
===========================

:Author: Samuel Warfield

Dependencies
------------

- Latex (Recommend tex studio to kill most Dependencies)
- Metropolis Beamer theme
- minted
- algorithm2e
- ifoddpage

Topics Covered
--------------

Basics

- DNS
- Mines CGI and wordpress Server
- Self Hosting
- AWS/GC/DO?
- Sphinx & Pelican
